Topmenu global pour tous les sites et services infini.fr
========================================================


Les entrées du menu sont ajoutées via le plugin Menus (identifiant: topmenu) dans le site SPIP.

Il est appelé ainsi dans le SPIP:

    <nav class="topmenu no-mobile">
        #INCLURE{fond=inclure/menu, env, identifiant=topmenu}
    </nav>

Et ainsi dans les autres sites:

    <!-- Id pour le menu responsive -->
    <nav id="menu" class="topmenu">
        <a href="#menu" class="topmenu-bouton open">MENU</a>
        <a href="#" class="topmenu-bouton close">MENU</a>
        <!-- Le menu est inclus ici (remplace id=topmenu)-->
        <div id="topmenu"></div>
        <script src="http://www.infini.fr/spip.php?page=topmenu.js" type="text/javascript"></script>
    </nav>


C'est une adaptation de ce qui est utilisé pour la topnav des sites de la galaxie SPIP.

cf https://zone.spip.org/trac/spip-zone/browser/_galaxie_/boussole.spip.net/squelettes/spipnav.js.html

Liste des fichiers :
--------------------
* topmenu.css : Styles du menu
* topmenu_spip-admin.css : Styles pour les boutons d'administration de SPIP
* fonts : Dossier contenant la font DejaVu Sans (optionnel)
* icons.css : les icones (utilisés aussi dans le reste du site SPIP)
* topmenu.js.html

Tout les fichiers sont dans https://infini.fr/topmenu/.

Démo sur http://www.ifaidy.infini.fr/travaux/spip/

Style piqué à Framasoft


Liste des classes :
-------------------
* spip_out : pour les liens sortants
* custom-icon-inline : affiche une icone, avec la classe correspondante dans le fichier icons.css

Les "bulles" d'infos:
---------------------
Utiliser la classe "details": à mettre sur une entree en texte libre placé en enfant de l'entree concernée (bouton "Créer sous-menu" > "Ajouter une entrée" > "Texte libre").
Le titre dans un `<h3 class="titre">` et le texte dans un `<p>`
